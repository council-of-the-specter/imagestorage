import 'package:flutter/material.dart';

class DefaultAppBar extends AppBar {
  DefaultAppBar({Key key})
      : super(
    key: key,
    title: Text("Image Storage App"),
    centerTitle: true,
    automaticallyImplyLeading: false,
    backgroundColor: Colors.grey,
    actions: null
  );
}